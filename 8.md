### 二、 树节点结构的构思和设计（6）Put系列方法之put_arrayslice

### 2.  BsTr结构体的Put系列方法之put_arrayslice

上一节讲了map存入BsTr结构的方法，主要是ID和des字段的处理，因为map存在key和value两个因素。

这一节谈谈数组和切片存入BsTr结构的方法。
- #### 先讲讲涉及的go语言基础知识：

- go语言数组是值类型，切片是引用类型，是动态数组，是对数组的封装，它包含一个指向固定数组的指针，并具有长度和容量字段，因此在64为机器上，一个nil切片占用24个字节，即指针、长度和容量各占8位。至于go语言是如何实现切片，这里不细究，只要明白它的动态性和这三个要素有关，指针指向的固定数组会随着长度的增加而从新分配。

因此，使用切片的时候，要非常注意，传递切片的时候，传的是引用，或者说是传的是三要素所组成的结构体的值，一旦底层数组在其他地方发生改变，传来的这里必然发生改变，这就会出现bug。比如一棵有序的树，关键字是[]byte类型的切片，但是关键字的来源是同一个字符串的不同索引之间的子字符串，一旦源字符串发生变化，[]byte类型关键字也将发生变化，而[]byte类型关键字经过比较后在树中的排序次序是不会改变的，这将导致本来有序的关键字乱序了。这种情况下，应当深度copy切片，再进行传递，或者使用数组不会存在这个问题。

- 数组是值类型，声明一个数组后，数组的元素将使用对应类型的默认值。声明一个数组或给一个数组赋值，形如：

```
var iarr [5]int //声明一个5个元素的int数组，默认值为0
var farr [10]float32 //声明一个10个元素的float32数组，默认值为0.0
arr := [5]int{1,2,3,4,5} //声明一个5个元素的int数组并赋值，其中长度可以省略，
                         //用"..."来代替，go语言会根据元素的个数来自动计算长度。
arr := [...]int{1,2,3,4,5}
```
- 切片是引用类型，声明一个切片或者初始化一个切片，形如：

```
slice1 := []int{1, 2, 3}  //可以声明一个未指定大小的数组来定义切片
slice2 := make([]int, 3)  //则使用make()函数来创建切片
var arr1 := [3]int{1,2,3，4,5} 
slice3 := arr1[1:4]  //通过对数组的引用来定义切片,注意这里中括号是左闭右开的区间
slice4 := arr1[:] //引用整个数组
slice5 := make([]int, 3, 4) //也可以指定容量，其中第三个参数capacity为可选参数。
var slice6 []int //这是一个nil切片，但和空切片一样，可以append数据。
slice7 := []int{2, 5: 19, 4}  //将第5个元素指定为19,紧接着第6个元素为4，即slice7 为 [2 0 0 0 0 19 4]。建议不要用此方法。
```
- append

```
slice1  = append(slice1 ,4) //slice1为[1 2 3 4]
```
- 清空切片
```
slice1 = slice1[:0] //但是要注意，这并没有清空底层数组的数据。
```
- 删除元素，使用内的copy函数实现，或者1.21.0版本后，go语标准库加入了新的slices包，提供了删除函数。

```
l := len(slice4) //获取slice4的长度
copy(slice4[:2],slice4[3:]) // 覆盖了index为2的元素,即删除了
slice4 = slice4 [:l-1] //从截断尾部最后一个元素
```
- ####  切片使用起来非常灵活，基本上固定数组已经很少使用，但是切片的使用一定要注意安全。

### 下面进入put_arrayslice的正题，先上代码：


```
func (cs *BsTr) put_arrayslice(rv reflect.Value, fu_type_str string, tag string, key *string, inittm int64) error {
	var e error

	if key == nil {
		ikey, _, _, _ := IDxyl(inittm)
		if ikey == 0 {
			return ErrIdxy
		}
		tkey := strconv.FormatInt(ikey, 36)
		key = &tkey
	}
	rvtypestring := rv.Type().String()
	k := rv.Kind()
	switch k {
	case reflect.Array, reflect.Slice:
		l := rv.Len()
		if l > 0 {
			switch rv.Index(0).Kind() {
			case reflect.Uint8: //[]byte，短路操作
				e = cs.PutBytes(rv.Bytes(), *key, "", fu_type_str) 
				if e != nil {
					return e
				}
			default:
				for i := 0; i < l; i++ {
					tkey := strconv.FormatInt(int64(i), 36) + ":" + *key
					e = cs.put_arrayslice(rv.Index(i), fu_type_str, tag, &tkey, inittm)
					if e != nil {
						return e
					}
				}
			}
		}
	case reflect.Bool:
		e = cs.PutBool(rv.Bool(), *key, "", fu_type_str)
		if e != nil {
			return e
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		e = cs.PutInt64(rv.Int(), *key, "", fu_type_str)
		if e != nil {
			return e
		}
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		e = cs.PutUint64(rv.Uint(), *key, "", fu_type_str)
		if e != nil {
			return e
		}
	case reflect.Float32, reflect.Float64:
		e = cs.PutFloat64(rv.Float(), *key, "", fu_type_str)
		if e != nil {
			return e
		}
	case reflect.Complex64, reflect.Complex128:
		e = cs.PutComplex128(rv.Complex(), *key, "", fu_type_str)
		if e != nil {
			return e
		}
	case reflect.String:
		e = cs.PutString(rv.String(), *key, "", fu_type_str)
		if e != nil {
			return e
		}
	case reflect.Struct:
		e = cs.put_struct(rv, fu_type_str+":"+rvtypestring, tag, key, inittm)
		if e != nil {
			return e
		}
	case reflect.Map:
		e = cs.put_map(rv, fu_type_str+":"+rvtypestring, tag, key, inittm)
		if e != nil {
			return e
		}
	case reflect.Interface:
		if !rv.CanInterface() {
			return ErrCaninterface
		}
		_, e = cs.putAnyByKey(rv.Interface(), fu_type_str+":"+rvtypestring, tag, key, inittm)
		if e != nil {
			return e
		}
	default:
		return ErrNotsupportedtype
	}

	return nil
}
```

- 关键如下代码，如果反射出来的是数组或者切片，先判断第1个元素是否为uint8，因为byte是uint8的别称，因此可以判断这是个[]byte,直接调用PutBytes方法即可，否则对数组或切片的每个元素进行递归调用put_arrayslice，直至遇到非数组或切片类型，调用相应类型的put函数。

`tkey := strconv.FormatInt(int64(i), 36) + ":" + *key`，存入BsTr结构字段的ID为逆向的数组或切片相应维度的index，key为包含该切片的结构体类型名和IDxyl生成ID的逆向分割字符串。即对于数组`var a [3][2][1]int`,对于元素`[0][1][0]`,其ID为`0:1:0：a:结构体变量名：...:IDxyl函数生成的ID`。

```
case reflect.Array, reflect.Slice:
		l := rv.Len()
		if l > 0 {
			switch rv.Index(0).Kind() {
			case reflect.Uint8: //[]byte，短路操作
				e = cs.PutBytes(rv.Bytes(), *key, "", fu_type_str) 
				if e != nil {
					return e
				}
			default:
				for i := 0; i < l; i++ {
					tkey := strconv.FormatInt(int64(i), 36) + ":" + *key
					e = cs.put_arrayslice(rv.Index(i), fu_type_str, tag, &tkey, inittm)
					if e != nil {
						return e
					}
				}
			}
		}
```
