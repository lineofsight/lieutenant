### 二、 树节点结构的构思和设计（6）Put/Get系列方法应用示例

### 3.  BsTr结构体的Put/Get系列方法应用示例

先上代码，


```
f, err := os.OpenFile("TestBstrPutGet", os.O_CREATE|os.O_RDWR, 0644) //文件拥有者对文件具有读写权利，其他用户只读
	if err != nil {
		log.Println("打开数据文件失败：", err)
	}
	defer f.Close()  //return之后执行关闭文件
	m := make(map[string]map[int]int)
	mi := make(map[int]int)
	mo := make(map[int]int)
	m["roytoyi"] = mi
	m["poouyyy"] = mo
	mi[9] = 100
	mo[10] = 1000
	ms := make(map[string]int)
	ms["dfgdg"] = 100000
	uu := UU{
		true,
		1,
		2,
		3,
		4,
		5,
		6,
		7,
		8,
		9,
		10,
		"sdklfsdk",
		[][]byte{{0}, {1}, {1, 2, 3}, {4, 5, 6, 7, 8}},
		[3]string{"55566", "3454", "234324"},
		[3][2][1]float32{{{11}, {12}}, {{13}, {14}}, {{15}, {16}}},
		ms,
		m,
		23334343 + 45645456i,
		UUU{[3]string{"uuu55566", "uuu3454", "uuu234324"},
			[3][2][1]float32{{{111}, {112}}, {{113}, {114}}, {{115}, {116}}},
			ms,
			m,
			111123334343 + 45645456i,
			nil,
			AUU{[3]string{"auuu55566", "auuu3454", "auuu234324"}, [3][2][1]float32{{{111}, {112}}, {{113}, {114}}, {{115}, {116}}}}},
	}
	var key string
	var cs BsTr
	cs.InitPool(f)  //初始化读写Pool等全局变量
	cs.InitWithLog("世界", "你好")  //初始化BsTr结构体，主要是对写入文件内容的长度进行初始化，此处还初始化了log文件、标识
	log.Println("--------------------------------------------测试：Put/Get structure/map etc.")  //写入log	
	// 向BsTr结构体变量存入100个uu结构体
        for i := 0; i < 100; i++ { 
                //k是ID，由IDxy()生成。IDxyl()采用类snowflake算法
	        //param 4 is nil, return a new key by auto
		k, e := cs.PutAnyByKey(uu, "", "json", nil, 1577808000000)
		log.Println(k)
		log.Println(e)
		log.Println(cs)
	}
	log.Println("-----------------------------------------测试：读取结构体等")
	ppcs := new(BsTr)  //新建一个BsTr结构体
	ppcs.InitWithoutBT() //初始化新建的BsTr结构体，bn、tn字段为空
	var e error
	k := ""
	//if &k==nil,key将自动生成
	//此时k为空,即key为空
        //将一个map[string]map[int]int类型的map存入BsTr结构体
	ppcs.PutAnyByKey(m, "", "json", &k, 1577808000000)
	log.Println(ppcs)
	mm := make(map[string]map[int]int)
        //从BsTr结构体读出map[string]map[int]int类型的map
        //k为空
	log.Println(ppcs.GetAnyByKey(k, &mm))
	log.Println(mm)
	ppcs = new(BsTr)
	ppcs.InitWithoutBT()
        //将一个三维数组存入BsTr结构体
	key, _ = ppcs.PutAnyByKey(uu.Afff, "", "json", &k, 1577808000000)
	log.Println(ppcs)
	var ffff [3][2][1]float32
        //从BsTr结构体读出[3][2][1]float32类型的三维数组
	log.Println(ppcs.GetAnyByKey("", &ffff))
	log.Println(ffff)
	ppcs = new(BsTr)
	ppcs.InitWithoutBT()
        //将一个[]byte切片写入BsTr结构体
	key, _ = ppcs.PutAnyByKey(uu.Ab, "", "json", &k, 1577808000000)
	log.Println(ppcs)
	var bb [][]byte
        //从BsTr结构体读出[]byte切片
	log.Println(ppcs.GetAnyByKey("", &bb))
	log.Println(bb)
	ppcs = new(BsTr)
	ppcs.InitWithoutBT()
        //将一个UU结构体写入BsTr结构体
	ppcs.PutAnyByKey(uu, "", "json", &k, 1577808000000)
	log.Println(ppcs)
        //将一个BsTr结构体写入文件
	log.Println(ppcs.Save(0, f))
        //根据BsTr结构体的offset字段，从文件读出一个BsTr结构体
	tppcs, _, e := GetFromFileOff(ppcs.Offset(), f)
	log.Println(tppcs)
	log.Println(e)
        //比较读出的BsTr结构体和源BsTr结构体是否相同
	log.Println(reflect.DeepEqual(tppcs, ppcs))
	var gcsuu, gcsuu4 UU
        //从源BsTr结构体读出一个UU结构体
	log.Println(ppcs.GetAnyByKey("", &gcsuu))
        //从文件读出的BsTr结构体中读出一个UU结构体
	log.Println(tppcs.GetAnyByKey("", &gcsuu4))
        //比较两个UU结构体是否相同
	log.Println(reflect.DeepEqual(gcsuu, gcsuu4))
	//struct field name
	//key is ""
        //从BsTr结构体读出UU结构体的Abool字段
	b, _ := ppcs.GetBool("Abool")
	log.Println(*b)
	log.Println("uu", uu)
	log.Println("gcsuu", gcsuu)
        //比较写入BsTr结构体的UU结构体，和从BsTr结构体读出的UU结构体是否相同
	log.Println(reflect.DeepEqual(gcsuu, uu))
	//use sort carefully
	sort.Sort(ppcs)
	log.Println(ppcs)
	log.Println(ppcs.GetAnyByKey(key, &gcsuu4))
	log.Println("uu", uu)
	log.Println("gcsuu4", gcsuu4)
	log.Println(reflect.DeepEqual(gcsuu4, uu))
	log.Println("--------------------key非空------------------")
	ppcs = new(BsTr)
	ppcs.InitWithoutBT()
	key, _ = ppcs.PutAnyByKey(m, "", "json", nil, 1577808000000)
	log.Println(ppcs)
	mm = make(map[string]map[int]int)
        //key由雪花算法生成
	log.Println(ppcs.GetAnyByKey(key, &mm))
	log.Println(mm)
	ppcs = new(BsTr)
	ppcs.InitWithoutBT()
	key, _ = ppcs.PutAnyByKey(uu.Afff, "", "json", nil, 1577808000000)
	log.Println(ppcs)
	log.Println(ppcs.GetAnyByKey(key, &ffff))
	log.Println(ffff)
	ppcs = new(BsTr)
	ppcs.InitWithoutBT()
	key, _ = ppcs.PutAnyByKey(uu.Ab, "", "json", nil, 1577808000000)
	log.Println(ppcs)
	log.Println(ppcs.GetAnyByKey(key, &bb))
	log.Println(bb)
	ppcs = new(BsTr)
	ppcs.InitWithoutBT()
        //第四个参数为nil，PutAnyByKey将通过雪花算法生成一个key
	key, _ = ppcs.PutAnyByKey(uu, "", "json", nil, 1577808000000)
	log.Println(ppcs)
	var gcsuu3, gcsuu1, gcsuu2 UU
        //通过生成的key从BsTr结构体读出一个UU结构体
	log.Println(ppcs.GetAnyByKey(key, &gcsuu3))
        //通过字段+：+key，从BsTr结构体读出UU结构体的一个字段的值（指针）
	b, _ = ppcs.GetBool("Abool:" + key)
	log.Println(*b)
	log.Println("uu", uu)
	log.Println("gcsuu", gcsuu3)
	log.Println(reflect.DeepEqual(gcsuu3, uu))
	key1, _ := ppcs.PutAnyByKey(uu, "", "json", nil, 1577808000000)
	log.Println(ppcs)
	log.Println(ppcs.GetAnyByKey(key1, &gcsuu2))
	log.Println(ppcs.GetAnyByKey(key, &gcsuu1))
	log.Println("gcsuu21", gcsuu1)
	log.Println("gcsuu2", gcsuu2)
	log.Println(reflect.DeepEqual(gcsuu2, gcsuu1))
	log.Println(reflect.DeepEqual(uu, gcsuu1))
	log.Println("-----------------------------------------测试：加入指针Put/Get")
	gcsuu.Auuu.Puu = &uu
	log.Println("gcsuu.Auuu.Puu", gcsuu.Auuu.Puu)
	ppcs = new(BsTr)
	ppcs.InitWithoutBT()
	k, e = ppcs.PutAnyByKey(gcsuu, "", "json", &key, 1577808000000)
	log.Println(key, ":", k)
	log.Println(e)
	log.Println(ppcs)
	var pgcsuu UU
	e = ppcs.GetAnyByKey(k, &pgcsuu)
	log.Println(e)
	log.Println(pgcsuu)
	b, _ = ppcs.GetBool("Abool:" + k)
	log.Println(*b)
	log.Println(gcsuu)
	log.Println(gcsuu.Auuu.Puu)
	//gcsuu.Auuu.Puu = &uu，并不支持从BsTr结构读出一个指向UU结构体的指针
        //因此以下代码比较会失败
	log.Println(reflect.DeepEqual(pgcsuu, gcsuu))
	log.Println(pgcsuu.Auuu.Puu)
```
- #### 涉及的go语言基础知识：
    
    - reflect.DeepEqual，用于深度比较两个复杂数据类型是否一致。

下一步讲对BsTr结构的Get系列方法进行详述。