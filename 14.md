### 二、 树节点结构的构思和设计（6）Get系列方法之get_array、get_ptr、get_struct

### 4.  BsTr结构体的Get系列方法之get_array、get_ptr、get_struct

1. get_array,先上代码，


```
func (cs *BsTr) get_array(key string, v reflect.Value) error {
	bfr := __uint8reg__.MatchString(v.Kind().String())
	switch bfr {
	case true: //[0-9]+byte,short circuit
		b, _ := cs.GetBytes(key)
		if b == nil {
			return ErrKeynotexist
		}
		v.SetBytes(*b)
	default:
		l := v.Len()
		if l > 0 {
			var keys string
			for i := 0; i < l; i++ {
				if key == "" {
					keys = strconv.FormatInt(int64(i), 36)
				} else {
					keys = strconv.FormatInt(int64(i), 36) + ":" + key
				}
				if e := cs.GetAnyByKey(keys, v.Index(i).Addr().Interface()); e != nil {
					return e
				}
			}
		}
	}
	return nil
}
```
- get_array比get_slice要简单。byte类型的一维数组短路操作和get_slice一样。但是其他情况的处理比get_slice简单很多，因为v已经包含了数组的长度信息，只需按索引号i递归调用GetAnyByKey。其中v的元素转为any，需要先获取其地址，即`v.Index(i).Addr().Interface()`。

2. get_ptr,先上代码


```
func (cs *BsTr) get_ptr(key string, v reflect.Value) error {
	var re_cs BsTr
	cs.FindContainsOrCompareID(key, &re_cs, false)
	if re_cs.lens[STRINGSN] > 0 {
		ta := reflect.New(v.Type().Elem())
		v.Set(ta)
		e := cs.GetAnyByKey(key, ta.Interface())
		if e != nil {
			return e
		}
		return nil
	}
	return ErrKeynotexist
}
```
- get_ptr并不是从BsTr结构获取指针，而是获取指针指向的内容。依然和前面的一样，re_cs这个BsTr结构的string切片存入所有包含key的字段即类型字符串。

`ta := reflect.New(v.Type().Elem())`，通过反射生成指针指向类型的元素；

`v.Set(ta)`，将元素设置到v；

`e := cs.GetAnyByKey(key, ta.Interface())`，从BsTr结构读取值到ta.Interface()，即ta的基础值。


3. get_struct,先上代码


```
func (cs *BsTr) get_struct(key string, v reflect.Value, t reflect.Type) error {
	var e error
	kind := v.Kind()
	switch kind {
	case reflect.Struct:
		n := t.NumField()
		for k := 0; k < n; k++ {
			if v.Field(k).IsValid() && t.Field(k).IsExported() {
				kind0 := v.Field(k).Kind()
				var keys string
				if key == "" {
					keys = t.Field(k).Name
				} else {
					keys = t.Field(k).Name + ":" + key
				}
				switch kind0 {
				case reflect.Bool:
					b, _ := cs.GetBool(keys)
					if b == nil {
						continue
					}
					v.Field(k).SetBool(*b)
				case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
					b, _ := cs.GetInt64(keys)
					if b == nil {
						continue
					}
					v.Field(k).SetInt(*b)
				case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
					b, _ := cs.GetUint64(keys)
					if b == nil {
						continue
					}
					v.Field(k).SetUint(*b)
				case reflect.Float32, reflect.Float64:
					b, _ := cs.GetFloat64(keys)
					if b == nil {
						continue
					}
					v.Field(k).SetFloat(*b)
				case reflect.Complex64, reflect.Complex128:
					b, _ := cs.GetComplex128(keys)
					if b == nil {
						continue
					}
					v.Field(k).SetComplex(*b)
				case reflect.String:
					b, _ := cs.GetString(keys)
					if b == nil {
						continue
					}
					v.Field(k).SetString(*b)
				case reflect.Struct:
					e = cs.get_struct(keys, v.Field(k), v.Field(k).Type())
					if e != nil {
						continue
					}
				case reflect.Interface, reflect.Map, reflect.Slice, reflect.Array, reflect.Ptr:
					if !v.Field(k).CanInterface() {
						continue
					}
					var va reflect.Value
					va = v.Field(k).Addr()
					e = cs.GetAnyByKey(keys, va.Interface())
					if e != nil {
						continue
					}
				default:
					continue
				}
			}
		}
	}
	return e
}
```

- 可以看出，首先获取`kind := v.Kind()`，判断是否为reflect.Struct,然后获取结构体内字段的数量（`n := t.NumField()`），开始遍历（遍历时首先判断是否为导出字段`v.Field(k).IsValid() && t.Field(k).IsExported()`，跳过非导出字段）。
- 遍历时首先获取字段索引号为k的值种类：`kind0 := v.Field(k).Kind()`，后缀字符串要将字段名加入，再判断其类型，如果为基础类型，直接从BsTr结构获取，然后设置到v.Field(k)。
- 遍历时如果v.Field(k).Kind()为reflect.Struct，则递归调用get_struct，使用新的累加的后缀字符串。
- 遍历时如果v.Field(k).Kind()为reflect.Interface, reflect.Map, reflect.Slice, reflect.Array, reflect.Ptr其中之一，则需要先判断能否获取基础值，即`if !v.Field(k).CanInterface() {
						continue
					}`，然后获取字段k的地址`va = v.Field(k).Addr()`，然后递归调用e = cs.GetAnyByKey(keys, va.Interface())，keys为使用新的累加的后缀字符串。

