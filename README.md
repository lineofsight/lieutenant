# 一个退役中校教你如何用go语言写一个基于B+树的json数据库

#### 介绍
如何用go语言实现B+树，并实现一个数据库。这个数据库解析json实现插入、删除、更新和查找。

#### 第一章  [自述学习go语言的历程](https://gitee.com/lineofsight/lieutenant/blob/master/1.md)
#### 第二章  B+树的节点结构
  #### 第一节  [树节点结构的构思和设计（1）](https://gitee.com/lineofsight/lieutenant/blob/master/2.md)
  #### 第二节  [树节点结构的构思和设计（2）](https://gitee.com/lineofsight/lieutenant/blob/master/3.md)
  #### 第三节  [树节点结构的构思和设计（3）](https://gitee.com/lineofsight/lieutenant/blob/master/4.md)
  #### 第四节  [树节点结构的构思和设计（4）](https://gitee.com/lineofsight/lieutenant/blob/master/5.md)
  #### 第五节  [树节点结构的构思和设计（5）之雪花算法](https://gitee.com/lineofsight/lieutenant/blob/master/6.md)
  #### 第六节  [树节点结构的构思和设计（6）](https://gitee.com/lineofsight/lieutenant/blob/master/7.md)
  #### 第七节  [树节点结构的构思和设计（7）](https://gitee.com/lineofsight/lieutenant/blob/master/8.md)
  #### 第八节  [树节点结构的构思和设计（8）](https://gitee.com/lineofsight/lieutenant/blob/master/9.md)
  #### 第九节  [树节点结构的构思和设计（9）](https://gitee.com/lineofsight/lieutenant/blob/master/10.md)
  #### 第十节  [树节点结构的构思和设计（10）](https://gitee.com/lineofsight/lieutenant/blob/master/11.md)
  #### 第十一节  [树节点结构的构思和设计（11）](https://gitee.com/lineofsight/lieutenant/blob/master/12.md)
  #### 第十二节  [树节点结构的构思和设计（12）](https://gitee.com/lineofsight/lieutenant/blob/master/13.md)
  #### 第十三节  [树节点结构的构思和设计（13）](https://gitee.com/lineofsight/lieutenant/blob/master/14.md)
  #### 第十四节  [树节点结构的构思和设计（14）](https://gitee.com/lineofsight/lieutenant/blob/master/15.md)
  #### 第十五节  [树节点结构的构思和设计（15）](https://gitee.com/lineofsight/lieutenant/blob/master/16.md)
  #### 第十六节  [树节点结构的构思和设计（16）](https://gitee.com/lineofsight/lieutenant/blob/master/17.md)
  #### 第十七节  [树节点结构的构思和设计（17）](https://gitee.com/lineofsight/lieutenant/blob/master/18.md)
  #### 第十八节  [树节点结构的构思和设计（18）](https://gitee.com/lineofsight/lieutenant/blob/master/19.md)
  #### 第十九节  [树节点结构的构思和设计（19）](https://gitee.com/lineofsight/lieutenant/blob/master/20.md)
  #### 第二十节  [树节点结构的构思和设计（20）](https://gitee.com/lineofsight/lieutenant/blob/master/21.md)
  #### 第二一节  [树节点结构的构思和设计（21）](https://gitee.com/lineofsight/lieutenant/blob/master/22.md)
  #### 第二二节  [树节点结构的构思和设计（22）](https://gitee.com/lineofsight/lieutenant/blob/master/23.md)
  #### 第二三节  [树节点结构的构思和设计（23）](https://gitee.com/lineofsight/lieutenant/blob/master/24.md)
  #### 第二四节  [树节点结构的构思和设计（24）](https://gitee.com/lineofsight/lieutenant/blob/master/25.md)
  #### 第二五节  [树节点结构的构思和设计（25）](https://gitee.com/lineofsight/lieutenant/blob/master/26.md)
  #### 第二六节  [树节点结构的构思和设计（26）](https://gitee.com/lineofsight/lieutenant/blob/master/27.md)
